package io.mcksn.cr.app;

import com.google.common.annotations.VisibleForTesting;
import io.mcksn.cr.domain.BettingSlip;
import io.mcksn.cr.domain.CurrentBettingSlips;
import io.mcksn.cr.domain.Wheel;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class App {

    public static Properties props;

    public static final String DEFAULT_PLAYERS_FILE_URI = "players";
    private final String PLAYERS_FILE_URI;

    private final InputStream inStrm;
    private final OutputStream outStrm;
    private final ExecutorService executorService;

    private final Wheel wheel;

    private Future<String> appFuture;

    @VisibleForTesting
    App(InputStream inputStream, OutputStream outputStream, Properties props) {
        App.props = props;
        PLAYERS_FILE_URI = App.props.getProperty("players.file.uri", DEFAULT_PLAYERS_FILE_URI);

        this.inStrm = inputStream;
        this.outStrm = outputStream;
        this.executorService = Executors.newFixedThreadPool(2);
        this.wheel = new Wheel(outputStream);

    }

    public static void main(String... args) throws IOException, ExecutionException, InterruptedException {

        Properties p = new Properties();
        p.load(App.class.getClassLoader().getResourceAsStream("app.properties"));
        new App(System.in, System.out, props).start().endWhenFinished(false);

    }

    @VisibleForTesting
    App start() throws IOException {

        Files.lines(Paths.get(PLAYERS_FILE_URI)); //loads player names as per requirements

        executorService.execute(wheel);

        appFuture = executorService.submit(() -> {
            String appEndResult = null;

            BufferedReader br = new
                    BufferedReader(new InputStreamReader(inStrm));
            PrintStream prStrm = new PrintStream(outStrm);

            String userInput = null;
            prStrm.println("Enter betting slip ...");
            prStrm.println("Enter 'quit' to quit.");

            do {
                try {
                    userInput = br.readLine();
                } catch (IOException e) {
                    appEndResult = e.getMessage();
                    break;
                }
                BettingSlip slip = new BettingSlip(userInput);
                CurrentBettingSlips.stampSlip(slip);
            } while (!userInput.equals("quit"));

            if (userInput.equals("quit")) {
                appEndResult = "quit";
            }

            return appEndResult;

        });
        return this;
    }

    /**
     * End game now or when finished.
     *
     * @param endNow the end now
     * @throws ExecutionException   the execution exception
     * @throws InterruptedException the interrupted exception
     */
    @VisibleForTesting
    void endWhenFinished(boolean endNow) throws ExecutionException, InterruptedException {
        if (!endNow) {
            String appResult = appFuture.get();
            System.out.println(appResult);
        }
        wheel.close();
        executorService.shutdownNow();
    }

}
