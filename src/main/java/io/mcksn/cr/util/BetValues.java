package io.mcksn.cr.util;


import io.mcksn.cr.domain.BetValue;
import io.mcksn.cr.domain.IntBetValue;
import io.mcksn.cr.domain.StringBetValue;

import java.util.ArrayList;
import java.util.List;


/**
 * Utility for BetValue
 */
public class BetValues {

    /**
     * Gets all possible bet values.
     *
     * @return the all possible
     */
    public static List<BetValue> getAllPossible() {
        List<BetValue> l = new ArrayList<>();
        l.addAll(StringBetValue.getAll());
        l.addAll(IntBetValue.getAllPossible());
        return l;
    }

}
