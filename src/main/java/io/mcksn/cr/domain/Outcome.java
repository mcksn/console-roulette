package io.mcksn.cr.domain;

import com.google.common.annotations.VisibleForTesting;

import java.text.MessageFormat;

/**
 * Outcome of betting slip from a game iteration
 */
public class Outcome {
    private final BettingSlip bettingSlip;
    private final PlayerStatus playerStatus;
    private final Double winnings;

    /**
     * Instantiates a new Outcome for a betting slip
     * base on position of ball
     *
     * @param bettingSlip the betting slip
     * @param ball        the ball
     */
    public Outcome(BettingSlip bettingSlip, Ball ball) {

        this.bettingSlip = bettingSlip;

        if (bettingSlip.getBetValue().isWinningSlip(ball)) {
            playerStatus = PlayerStatus.WINNER;
            //TODO looks ridiculous; maybe have calWinnings method on betting slip
            winnings = bettingSlip.getBetValue().calculateWinnings(bettingSlip.getAmount());
        } else {
            playerStatus = PlayerStatus.LOSER;
            winnings = 0D;
        }


    }

    public String toString() {
        //TODO create print method instead of toStrings
        return new StringBuilder()
                .append(bettingSlip.getPlayer().toString())
                .append(" ")
                .append(bettingSlip.getBetValue())
                .append(" ")
                .append(playerStatus.name())
                .append(" ")
                .append(MessageFormat.format("{0,number,#.#}", winnings))
                .toString();
    }

    private enum PlayerStatus {
        //TODO create underlying data
        WINNER,
        LOSER
    }

    @VisibleForTesting
    BettingSlip getBettingSlip() {
        return bettingSlip;
    }

    @VisibleForTesting
    Double getWinnings() {
        return winnings;
    }
}
