package io.mcksn.cr.domain;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Objects;

import java.util.Arrays;
import java.util.Collection;
import java.util.function.Predicate;

/**
 * String based bet value i.e. EVEN, ODD, RED, BLACK
 */
public class StringBetValue implements BetValue {

    private static Collection<StringBetValue> values =
            Arrays.asList(
                    new StringBetValue("EVEN", i -> i.getLandedOn() % 2 == 0),
                    new StringBetValue("ODD", i -> i.getLandedOn() % 2 != 0));

    private final Predicate<Ball> isWinningSlip;

    @VisibleForTesting
    final String value;

    public StringBetValue(String value, Predicate<Ball> isWinningSlip) {
        this.value = value;
        this.isWinningSlip = isWinningSlip;
    }

    public static Collection<? extends BetValue> getAll() {
        return values;
    }

    @Override
    public boolean isWinningSlip(Ball ball) {
        return isWinningSlip.test(ball);
    }

    @Override
    public Double calculateWinnings(Double amount) {
       return amount * 2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StringBetValue that = (StringBetValue) o;
        return Objects.equal(isWinningSlip, that.isWinningSlip) &&
                Objects.equal(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(isWinningSlip, value);
    }

    @Override
    public String toString() {
        return value;
    }
}
