package io.mcksn.cr.domain;

import io.mcksn.cr.util.BetValues;
import org.apache.commons.lang3.StringUtils;

/**
 * Betting slip. Details player's bet.
 */
public class BettingSlip {

    private final Player player;
    private final BetValue betValue;
    private final Double amount;

    /**
     * Expected format of string:
     *
     * "{player name} {bet value} {amount}"
     *
     * {bet value} - Integer (1-36),'EVEN','ODD'
     *
     * {amount} - Double
     *
     * @param bettingSlip the betting slip
     */
    public BettingSlip(String bettingSlip) {
        String[] arrayOfItems = StringUtils.split(bettingSlip);
        this.player = new Player(arrayOfItems[0]);
        this.betValue = BetValues.getAllPossible().stream().filter(bv -> bv.toString().equals(arrayOfItems[1])).findFirst().get();
        this.amount = Double.parseDouble(arrayOfItems[2]);
    }

    public Player getPlayer() {
        return player;
    }

    public BetValue getBetValue() {
        return betValue;
    }

    public Double getAmount() {
        return amount;
    }
}
