package io.mcksn.cr.domain;

import com.google.common.annotations.VisibleForTesting;
import io.mcksn.cr.app.App;

import java.io.OutputStream;
import java.io.PrintStream;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Game wheel. Directs game iteration.
 */
public class Wheel implements Runnable {

    //TODO play() while(true){spin() stop()}

    //thirty seconds
    public static final String DEFAULT_WHEEL_SPIN_TIME = "30000";

    //in milliseconds
    private int WHEEL_SPIN_TIME = Integer.parseInt(App.props.getProperty("wheel.spin.time", DEFAULT_WHEEL_SPIN_TIME));

    private final Ball ball = new Ball(IntBetValue.INCLUSIVE_RANGE_START, IntBetValue.INCLUSIVE_RANGE_END);
    private final PrintStream pStrm;

    private AtomicBoolean signalToClose = new AtomicBoolean(false);

    /**
     * Instantiates a new Wheel.
     *
     * @param outStrm the output stream for collecting messages to user.
     */
    public Wheel(OutputStream outStrm) {
        this.pStrm = new PrintStream(outStrm);
    }

    /**
     * Closes wheel from play.
     * Wheel#run() to re-open  wheel for play.
     */
    public synchronized void close() {

        //set before notify as signal checked directly after notify
        signalToClose.set(true);

        //wake wheel up from sleep/spinning state
        notify();
    }

    /**
     * Spins the wheel every WHEEL_SPIN_TIME. Ball is thrown into the wheel.
     * <p/>
     * When wheel stops outcome is printed.
     *
     * @throws InterruptedException the interrupted exception
     */
    public void spin() throws InterruptedException {
        signalToClose.set(false); //reset signal
        while (!signalToClose.get()) {
            synchronized (this) {
                ball.throww();
                wait(WHEEL_SPIN_TIME);
                ball.land();
                printOutcomes(ball);
            }
        }
    }

    /**
     * Prints outcome of betting slips given balls landing position.
     *
     * @param ball the ball
     */
    @VisibleForTesting
    void printOutcomes(Ball ball) {
        pStrm.println("Ball landed on " + ball.getLandedOn());
        pStrm.println("Player Bet Outcome Winnings");
        pStrm.println("-----");
        List<BettingSlip> slips = CurrentBettingSlips.getAndReset();
        if (slips.isEmpty()) {
            pStrm.println("No bets made");
        } else {
            slips.stream().forEach(s -> {
                Outcome outcome = new Outcome(s, ball);
                pStrm.println(outcome.toString());
            });
        }
        pStrm.println("-----");
    }
        @Override
        public void run () {
            try {
                spin();
            } catch (InterruptedException e) {
                System.err.println(e);
            }
        }
    }
