package io.mcksn.cr.domain;

import java.util.Random;

/**
 * Ball with awareness of its own position; changes state depending on how game interacts with it
 */
public class Ball {
    private final Random randomGenerator;
    private final Integer stoppablePosRangeStart;
    private final Integer stoppablePosRangeEnd;
    private Integer landedOn;

    /**
     * Instantiates a new Ball.
     *
     * @param stoppablePosRangeStart the stoppable position range start
     * @param stoppablePosRangeEnd   the stoppable position range end
     */
    public Ball(Integer stoppablePosRangeStart, Integer stoppablePosRangeEnd) {
        super();
        this.stoppablePosRangeEnd = stoppablePosRangeEnd;
        this.stoppablePosRangeStart = stoppablePosRangeStart;
        randomGenerator = new Random();
    }


    /**
     * Land on a position
     */
    public void land() {
        if (stoppablePosRangeEnd.equals(stoppablePosRangeStart)) {
            landedOn = stoppablePosRangeEnd;
        } else {
            int range = stoppablePosRangeEnd - stoppablePosRangeStart + 1;
            landedOn = randomGenerator.nextInt(range) + stoppablePosRangeStart;
        }
    }


    /**
     * Resets the landed on position.
     */

    public void throww() {
        landedOn = null;
    }

    /**
     * @return the landed on position.
     */
    public int getLandedOn() {
        return landedOn;
    }
}
