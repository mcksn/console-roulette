package io.mcksn.cr.domain;

import com.google.common.annotations.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Current betting slips of game iteration.
 */
public class CurrentBettingSlips {

    @VisibleForTesting
    static List<BettingSlip> currentSlips = new ArrayList<>(10);

    /**
     * Gets and reset current betting slips.
     *
     * @return the and reset
     */
    public synchronized static java.util.List<BettingSlip> getAndReset()
    {
        List<BettingSlip> slips = new ArrayList<>(currentSlips);
        currentSlips.clear();
        return slips;
    }

    /**
     * Add betting slip to betting slips collection to be checked at end of current game iteration.
     *
     * @param bettingSlip the betting slip
     */
    public synchronized static void stampSlip(BettingSlip bettingSlip)
    {
        currentSlips.add(bettingSlip);
    }
}
