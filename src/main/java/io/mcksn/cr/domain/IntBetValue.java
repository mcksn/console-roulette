package io.mcksn.cr.domain;

import com.google.common.base.Objects;

import java.util.Collection;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

/**
 * Integer based bet value i.e. 1, 4, 36
 */
public class IntBetValue implements BetValue {

    public final static int INCLUSIVE_RANGE_START = 1;
    public final static int INCLUSIVE_RANGE_END = 36;

    private static Collection<IntBetValue> values = IntStream.rangeClosed(INCLUSIVE_RANGE_START, INCLUSIVE_RANGE_END).mapToObj(IntBetValue::new).collect(toList());

    public static Collection<? extends BetValue> getAllPossible() {
        return values;
    }

    private final Integer value;

    public IntBetValue(int value) {
        this.value = value;
    }

    @Override
    public boolean isWinningSlip(Ball ball) {
        return java.util.Objects.equals(value, ball.getLandedOn());
    }

    @Override
    public Double calculateWinnings(Double amount) {
        return amount * 36.0D;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IntBetValue that = (IntBetValue) o;
        return Objects.equal(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    // TODO Use print instead
    @Override
    public String toString() {
        return value.toString();
    }
}
