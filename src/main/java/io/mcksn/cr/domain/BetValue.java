package io.mcksn.cr.domain;


/**
 * Value a player can make a bet with i.e. I bet this BetValue will appear
 */
public interface BetValue{

    /**
     * Is this a winning slip based on where the ball has landed.
     *
     * @param ball the ball
     * @return the boolean
     */
    boolean isWinningSlip(Ball ball);

    /**
     * Calculates winnings given player's bet amount.
     *
     * @param amount the amount
     * @return winnings
     */
    Double calculateWinnings(Double amount);
}
