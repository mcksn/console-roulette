package io.mcksn.cr.util;

import com.google.common.base.Predicates;
import io.mcksn.cr.domain.IntBetValue;
import io.mcksn.cr.domain.StringBetValue;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class BetValuesTest {

    @Test
    public void testGetAllPossibleStringTypeSize() throws Exception {

        //Given //When //Then
        assertThat(
                BetValues.getAllPossible()
                        .stream()
                        .filter(bv -> Predicates.instanceOf(StringBetValue.class).apply(bv))
                        .count(),
                is(2L));

    }

    @Test
    public void testGetAllPossibleIntTypeSize() throws Exception {

        //Given //When //Then
        assertThat(
                BetValues.getAllPossible()
                        .stream()
                        .filter(bv -> Predicates.instanceOf(IntBetValue.class).apply(bv))
                        .count(),
                is(36L));
    }
}