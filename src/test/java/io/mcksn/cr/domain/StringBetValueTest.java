package io.mcksn.cr.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class StringBetValueTest {

    @Test
    public void testIsWinningSlipGivenBallLandedOnEventBetValue() throws Exception {
        //Given
        Ball ball = new Ball(2, 2);
        ball.land();

        //When
        BetValue betValue = StringBetValue.getAll()
                .stream()
                .filter(bv -> ((StringBetValue) bv).value.equals("EVEN"))
                .findAny()
                .get();

        //Then
        assertTrue(betValue.isWinningSlip(ball));
    }

    @Test
    public void testIsWinningSlipGivenBallLandedOnOddBetValue() throws Exception {
        //Given
        Ball ball = new Ball(9, 9);
        ball.land();

        //When
        BetValue betValue = StringBetValue.getAll()
                .stream()
                .filter(bv -> ((StringBetValue) bv).value.equals("ODD"))
                .findAny()
                .get();

        //Then
        assertTrue(betValue.isWinningSlip(ball));
    }

    @Test
    public void testIsWinningSlipGivenBallDidNotLandedOnOddBetValue() throws Exception {

        //Given
        Ball ball = new Ball(2, 2);
        ball.land();

        //When
        BetValue betValue = StringBetValue.getAll()
                .stream()
                .filter(bv -> ((StringBetValue) bv).value.equals("ODD"))
                .findAny()
                .get();

        //Then
        assertFalse(betValue.isWinningSlip(ball));
    }


    @Test
    public void testIsWinningSlipGivenBallDidNotLandedOnEvenBetValue() throws Exception {

        //Given
        Ball ball = new Ball(3, 3);
        ball.land();

        //When
        BetValue betValue = StringBetValue.getAll()
                .stream()
                .filter(bv -> ((StringBetValue) bv).value.equals("EVEN"))
                .findAny()
                .get();

        //Then
        assertFalse(betValue.isWinningSlip(ball));
    }
    @Test
    public void testCalculateWinningsOutputAmountMultipliedBy2() throws Exception {

        //Given
        Double amount = 1D;
        BetValue betValue = StringBetValue.getAll().stream().findAny().get();

        //When
        Double actualWinnings = betValue.calculateWinnings(amount);

        //Then
        assertThat(actualWinnings, is(2D));
    }

    @Test
    public void testEqualsAndHashCode() throws Exception {
        EqualsVerifier.forClass(StringBetValue.class).usingGetClass().verify();
    }
}
