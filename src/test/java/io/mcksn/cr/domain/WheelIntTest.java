package io.mcksn.cr.domain;

import io.mcksn.cr.app.App;
import io.mcksn.cr.test.AbstractIntTest;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.apache.commons.lang3.StringUtils.contains;
import static org.apache.commons.lang3.StringUtils.containsAny;
import static org.junit.Assert.assertTrue;

public class WheelIntTest extends AbstractIntTest {

    //milliseconds
    public static final int ASSUMED_WHEEL_SPIN_TIME_PLUS_EXTRA_MILLIS = 12000;
    private Wheel wheel;

    @Override
    public void before() throws IOException {
        super.before();

        //reset static data
        CurrentBettingSlips.getAndReset();

        App.props = props;
    }

    @Override
    public void after() throws IOException {
        super.after();

        //reset static data
        CurrentBettingSlips.getAndReset();

        if (wheel != null)
            wheel.close();
    }

    @Test
    public void W_playTwoIterations_S_outputTwoOutcomes_G_betSlipFromSamePlayer() throws IOException, InterruptedException, ExecutionException {

        // Given
        outStream = createOutputStream();
        CurrentBettingSlips.stampSlip(new BettingSlip("ee 4 2.0"));
        wheel = new Wheel(outStream);
        Executor ex = Executors.newSingleThreadExecutor();

        //When
        ex.execute(wheel);
        Thread.sleep(ASSUMED_WHEEL_SPIN_TIME_PLUS_EXTRA_MILLIS);
        CurrentBettingSlips.stampSlip(new BettingSlip("ee 6 5.0"));
        Thread.sleep(ASSUMED_WHEEL_SPIN_TIME_PLUS_EXTRA_MILLIS);

        //Then
        assertTrue(EXPECTED_OUTCOME_NOT_IN_OUTPUT,
                contains(outStream.toString(), "ee 4 LOSER 0") || contains(outStream.toString(), "ee 4 WINNER 72.0"));
        assertTrue(EXPECTED_OUTCOME_NOT_IN_OUTPUT,
                contains(outStream.toString(), "ee 6 LOSER 0") || contains(outStream.toString(), "ee 6 WINNER 900.0"));
    }

}