package io.mcksn.cr.domain;

import io.mcksn.cr.app.App;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;

import static org.apache.commons.lang3.StringUtils.contains;
import static org.junit.Assert.assertTrue;

public class WheelTest {

    private Wheel wheel = null;
    private OutputStream outStream = null;
    private Ball ball = null;

    @Before
    public void before() throws IOException {

        Properties props = new Properties();
        props.load(App.class.getClassLoader().getResourceAsStream("app.properties"));

        App.props = props;

        outStream = new ByteArrayOutputStream();

        CurrentBettingSlips.getAndReset();

    }

    @After
    public void after() throws IOException {

        outStream.close();

        CurrentBettingSlips.getAndReset();
    }

    @Test
    public void testPrintOutcomesWithOneOutcome() {

        //Given
        wheel = new Wheel(outStream);
        ball = new Ball(1, 1);
        ball.land();
        CurrentBettingSlips.stampSlip(new BettingSlip("ee 4 4.0"));

        //When
        wheel.printOutcomes(ball);

        //Then
        assertTrue("expected outcome not in output", contains(outStream.toString(), "ee 4 LOSER 0"));
        assertTrue("ball landed not in output", contains(outStream.toString(), "Ball landed on 1"));
        assertTrue("titles not in output", contains(outStream.toString(), "Player Bet Outcome Winning"));

    }

    @Test
    public void testPrintOutcomesWithTwoOutcomes() {

        //Given
        wheel = new Wheel(outStream);
        ball = new Ball(1, 1);
        ball.land();
        CurrentBettingSlips.stampSlip(new BettingSlip("ee 4 4.0"));
        CurrentBettingSlips.stampSlip(new BettingSlip("ff 4 4.0"));

        //When
        wheel.printOutcomes(ball);

        //Then
        assertTrue("expected outcome not in output", contains(outStream.toString(), "ee 4 LOSER 0"));
        assertTrue("expected outcome not in output", contains(outStream.toString(), "ff 4 LOSER 0"));
        assertTrue("ball landed not in output", contains(outStream.toString(), "Ball landed on 1"));
        assertTrue("titles not in output", contains(outStream.toString(), "Player Bet Outcome Winning"));
    }

    @Test
    public void testPrintOutcomesWithNoOutcomes() {
        //Given
        wheel = new Wheel(outStream);
        ball = new Ball(1, 1);
        ball.land();

        //When
        wheel.printOutcomes(ball);

        //Then
        assertTrue("expected outcome not in output", contains(outStream.toString(), "No bets made"));
        assertTrue("ball landed not in output", contains(outStream.toString(), "Ball landed on 1"));
        assertTrue("titles not in output", contains(outStream.toString(), "Player Bet Outcome Winning"));
    }
}