package io.mcksn.cr.domain;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class BettingSlipTest {

    @Test
    public void testInitialize() {

        //Given //When
        BettingSlip slip = new BettingSlip("ee 3 3.0");

        //Then
        assertThat("player not as expected", slip.getPlayer(), is(new Player("ee")));
        assertThat("amount not as expected", slip.getAmount(), is(3D));
        assertThat("bet value not as expected", slip.getBetValue(), is(new IntBetValue(3)));

    }

}