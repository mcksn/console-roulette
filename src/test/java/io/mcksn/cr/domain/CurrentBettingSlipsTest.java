package io.mcksn.cr.domain;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CurrentBettingSlipsTest {

    @Test
    public void testGetAndReset() throws Exception {

        //Given
        BettingSlip bettingSlip = new BettingSlip("ee 4 5.0");
        CurrentBettingSlips.currentSlips.add(bettingSlip);

        //When
        List<BettingSlip> expectedSlips = CurrentBettingSlips.getAndReset();

        //Then
        assertTrue("expected slip not in collection",    expectedSlips.containsAll(Arrays.asList(bettingSlip)));
        assertFalse("current slips not reset", CurrentBettingSlips.currentSlips.contains(bettingSlip));

    }

    @Test
    public void testStampSlip() throws Exception {

        //Given
        BettingSlip bettingSlip = new BettingSlip("ee 4 5.0");

        //When
        CurrentBettingSlips.stampSlip(bettingSlip);

        //Then
        assertTrue("expected slip not in collection", CurrentBettingSlips.currentSlips.contains(bettingSlip));
    }
}