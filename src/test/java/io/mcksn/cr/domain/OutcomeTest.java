package io.mcksn.cr.domain;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class OutcomeTest {

    @Test
    public void testInitializeGivenBetLoses()
    {
        //Given
        Ball ball = new Ball(1,1);
        ball.land();

        //When
        Outcome actualOutcome = new Outcome(new BettingSlip("ss 4 2.2"), ball );

        //Then
        assertThat("winnings not as expected", actualOutcome.getWinnings(),is(0D));
        assertThat("bet value not as expected", actualOutcome.getBettingSlip().getBetValue(), is(new IntBetValue(4)));
        assertThat("player not as expected", actualOutcome.getBettingSlip().getPlayer(),is(new Player("ss")) );
    }

    @Test
    public void testInitializeGivenBetWinning()
    {
        //Given
        Ball ball = new Ball(2,2);
        ball.land();

        //When
        Outcome actualOutcome = new Outcome(new BettingSlip("ss 2 2.2"), ball );

        //Then
        assertThat("winnings not as expected", actualOutcome.getWinnings(),is(79.2D));
        assertThat("bet value not as expected", actualOutcome.getBettingSlip().getBetValue(), is(new IntBetValue(2)));
        assertThat("player not as expected", actualOutcome.getBettingSlip().getPlayer(),is(new Player("ss")) );

    }
}