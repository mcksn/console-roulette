package io.mcksn.cr.domain;

import nl.jqno.equalsverifier.EqualsVerifier;
import org.hamcrest.core.Is;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class IntBetValueTest {

    @Test
    public void testIsWinningSlipGivenBallLandedOnBetValue() throws Exception {
        //Given
        Ball ball = new Ball(9, 9);
        ball.land();

        //When
        IntBetValue betValue = new IntBetValue(9);

        //Then
        assertTrue(betValue.isWinningSlip(ball));
    }

    @Test
    public void testIsWinningSlipGivenBallDidNotLandOnBetValue() throws Exception {

        //Given
        Ball ball = new Ball(2, 2);
        ball.land();

        //When
        IntBetValue betValue = new IntBetValue(9);

        //Then
        assertFalse(betValue.isWinningSlip(ball));
    }

    @Test
    public void testCalculateWinningsOutputAmountMultipliedBy36() throws Exception {

        //Given
        Double amount = 1D;
        IntBetValue betValue = new IntBetValue(1);

        //When
        Double actualWinnings = betValue.calculateWinnings(amount);

        //Then
        assertThat(actualWinnings, is(36D));
    }

    @Test
    public void testEqualsAndHashCode() throws Exception {
        EqualsVerifier.forClass(IntBetValue.class).usingGetClass().verify();
    }
}