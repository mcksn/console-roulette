package io.mcksn.cr.test;

import io.mcksn.cr.app.App;
import org.junit.After;
import org.junit.Before;

import java.io.*;
import java.nio.charset.Charset;
import java.util.Properties;

public abstract class AbstractIntTest {

    public static final String EXPECTED_OUTCOME_NOT_IN_OUTPUT = "expected outcome not in output";
    protected Properties props;
    protected OutputStream outStream;
    protected InputStream inStream;

    @Before
    public void before() throws IOException {
        props = new Properties();
        props.load(App.class.getClassLoader().getResourceAsStream("app.properties"));
    }


    @After
    public void after() throws IOException {
        if (inStream != null)
            inStream.close();

        if (outStream != null)
            outStream.close();
    }

    protected OutputStream createOutputStream() {
        return new ByteArrayOutputStream();
    }

    protected InputStream createInputStreamWith(String initialStringOnStream) {
        return new ByteArrayInputStream(initialStringOnStream.getBytes(Charset.defaultCharset())) {
        };
    }
}
