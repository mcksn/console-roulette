package io.mcksn.cr.app;

import io.mcksn.cr.test.AbstractIntTest;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.ExecutionException;

import static org.apache.commons.lang3.StringUtils.contains;
import static org.junit.Assert.assertTrue;

/**
 * Integration tests for Console Roulette
 */
public class AppIntTest extends AbstractIntTest {

    @Test
    public void W_play_S_outputOutcome_G_betSlip() throws IOException, ExecutionException, InterruptedException {

        // Given
        OutputStream outStream = createOutputStream();
        InputStream inStream = createInputStreamWith("ee 3 2.0");

        //When
        App app = new App(inStream, outStream, props).start();
        Thread.sleep(15000);
        app.endWhenFinished(true);

        //Then
        assertTrue(EXPECTED_OUTCOME_NOT_IN_OUTPUT,
                contains(outStream.toString(), "ee 3 LOSER 0") || contains(outStream.toString(), "ee 3 WINNER 72.0"));

    }

    @Test
    public void W_play_S_outputTwoOutcomes_G_twoBetSlipFromDiffPlayers() throws InterruptedException, ExecutionException, IOException {

        // Given
        outStream = createOutputStream();
        inStream = createInputStreamWith("ee 3 2.0\nff 4 3.0");

        //When
        App app = new App(inStream, outStream, props).start();
        Thread.sleep(15000);
        app.endWhenFinished(true);

        //Then
        assertTrue(EXPECTED_OUTCOME_NOT_IN_OUTPUT,
                contains(outStream.toString(), "ee 3 LOSER 0") || contains(outStream.toString(), "ee 3 WINNER 72.0"));
        assertTrue(EXPECTED_OUTCOME_NOT_IN_OUTPUT,
                contains(outStream.toString(), "ff 4 LOSER 0") || contains(outStream.toString(), "ff WINNER 108.0"));
    }

    @Test
    public void W_play_S_outputTwoOutcomes_G_twoBetSlipFromSamePlayer() throws InterruptedException, IOException, ExecutionException {

        // Given
        outStream = createOutputStream();
        inStream = createInputStreamWith("ee 3 2.0\nee 4 3.0");

        //When
        App app = new App(inStream, outStream, props).start();
        Thread.sleep(15000);
        app.endWhenFinished(true);

        //Then
        assertTrue(EXPECTED_OUTCOME_NOT_IN_OUTPUT,
                contains(outStream.toString(), "ee 3 LOSER 0") || contains(outStream.toString(), "ee 3 WINNER 72.0"));
        assertTrue(EXPECTED_OUTCOME_NOT_IN_OUTPUT,
                contains(outStream.toString(), "ee 4 LOSER 0") || contains(outStream.toString(), "ee  WINNER 108.0"));
    }
}
